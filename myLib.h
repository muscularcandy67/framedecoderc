#ifndef MYLIB_H_INCLUDED
#define MYLIB_H_INCLUDED

typedef struct {
    unsigned char bytes[6];
} mac_addr; 

int htoi (char strHex[]);


int fgetFrame(char* szEthFrame, char* filename);  //return number of byte readed from file 
/** **/
int copyBytesFromBuf(char* szTypeLen, char* szEthFrame,  int idx, int numByte); //return <0 if szEthFrame ends before numByte

int getEthType(char* framebuf);	//return EthType/Length field
void getDestMac(mac_addr mac,char* framebuf);
void getSrcMac(mac_addr mac,char* framebuf);

void printMac(mac_addr mac);      //stampa il mac e lo caratterizza
int decodeLLC(char* framebuf); //decodifica LLC stampando le info (SAP, OUI, ecc) e ritorna DSAP o EthType se presente 
int checkFrameLenght(char* frame);//controlla lunghezza del frame con lenght

#endif // MYLIB_H_INCLUDED
